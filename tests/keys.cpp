#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "../SRKeys.hpp"

TEST_CASE( "Testing conversion key id to string" ) {
	CHECK( SRKeys::getKey( 0x12 ) == "VK_MENU" );
	CHECK( SRKeys::getKey( 'A' ) == "A" );
	CHECK( SRKeys::getKey( '9' ) == "9" );
	CHECK( SRKeys::getKey( 0 ) == "&0" );
}

TEST_CASE( "Testing conversion key tring to key id" ) {
	CHECK( SRKeys::getKey( "VK_MENU" ) == 0x12 );
	CHECK( SRKeys::getKey( "A" ) == 'A' );
	CHECK( SRKeys::getKey( "9" ) == '9' );
	CHECK( SRKeys::getKey( "&0" ) == 0 );
	CHECK( SRKeys::getKey( "<32>" ) == 32 );
}