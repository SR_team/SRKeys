#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "../SRKeys.hpp"

TEST_CASE( "Testing create combo" ) {
	CHECK( SRKeys::createCombo( "VK_LSHIFT+VK_LBUTTON", '+' ) == std::vector<int>{ 0xA0, 0x01 } );
	CHECK( SRKeys::createCombo( "VK_LMENU|A", '|' ) == std::vector<int>{ 0xA4, 'A' } );
	CHECK( SRKeys::createCombo( "VK_LMENU VK_LSHIFT <1337>", ' ' ) == std::vector<int>{ 0xA4, 0xA0, 1337 } );
	CHECK( SRKeys::createCombo( "<1>,<2>,<3>,", ',' ) == std::vector<int>{ 1, 2, 3 } );
	CHECK( SRKeys::createCombo( "<1>,<2>,<3>,,,,", ',' ) == std::vector<int>{ 1, 2, 3 } );
	CHECK( SRKeys::createCombo( "<1>,<2>,<3>,,,,,", ',' ) == std::vector<int>{ 1, 2, 3 } );
	CHECK( SRKeys::createCombo( "rstb.ty...nm.um.teb..y.tui", '.' ) == std::vector<int>{} );
}

TEST_CASE( "Testing conversion combo to string" ) {
	CHECK( SRKeys::combo2string( { 0xA0, 0x01 }, '+' ) == "VK_LSHIFT+VK_LBUTTON" );
	CHECK( SRKeys::combo2string( { 0xA4, 'A' }, '|' ) == "VK_LMENU|A" );
	CHECK( SRKeys::combo2string( { 0xA4, 0xA0, 1337 }, ' ' ) == "VK_LMENU VK_LSHIFT <1337>" );
}

TEST_CASE( "Testing combo validation" ) {
	auto validCombo = std::vector<int>{ 0xA4, 0xA0, 1337 };
	auto invalidCombo = std::vector<int>{ 0xA4, 0, 1337 };

	CHECK( SRKeys::isComboValid( validCombo ) );
	CHECK_FALSE( SRKeys::isComboValid( invalidCombo ) );
}