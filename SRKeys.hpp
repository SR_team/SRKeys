#pragma once

#include <cstdint>
#include <string>
#include <string_view>
#include <vector>
#include <algorithm>
#include <array>
#include <charconv>

namespace SRKeys {
	constexpr auto kInvalidKeyName = "&0";
	namespace details {
		struct key_alias {
			int key;
			const char *name;
		};
		static constexpr std::array<key_alias, 101> key_alias{ {
			{ 0x01, "VK_LBUTTON" },
			{ 0x02, "VK_RBUTTON" },
			{ 0x04, "VK_MBUTTON" },
			{ 0x05, "VK_XBUTTON1" },
			{ 0x06, "VK_XBUTTON2" },
			{ 0x08, "VK_BACK" },
			{ 0x09, "VK_TAB" },
			{ 0x14, "VK_CAPITAL" },
			{ 0x0D, "VK_RETURN" },
			{ 0x10, "VK_SHIFT" },
			{ 0xA0, "VK_LSHIFT" },
			{ 0xA1, "VK_RSHIFT" },
			{ 0x11, "VK_CONTROL" },
			{ 0xA2, "VK_LCONTROL" },
			{ 0xA3, "VK_RCONTROL" },
			{ 0x12, "VK_MENU" },
			{ 0xA4, "VK_LMENU" },
			{ 0xA5, "VK_RMENU" },
			{ 0x20, "VK_SPACE" },
			{ 0x21, "VK_PRIOR" },
			{ 0x22, "VK_NEXT" },
			{ 0x23, "VK_END" },
			{ 0x24, "VK_HOME" },
			{ 0x25, "VK_LEFT" },
			{ 0x26, "VK_UP" },
			{ 0x27, "VK_RIGHT" },
			{ 0x28, "VK_DOWN" },
			{ 0x2D, "VK_INSERT" },
			{ 0x2E, "VK_DELETE" },
			{ 0x13, "VK_PAUSE" },
			{ 0x60, "VK_NUMPAD0" },
			{ 0x61, "VK_NUMPAD1" },
			{ 0x62, "VK_NUMPAD2" },
			{ 0x63, "VK_NUMPAD3" },
			{ 0x64, "VK_NUMPAD4" },
			{ 0x65, "VK_NUMPAD5" },
			{ 0x66, "VK_NUMPAD6" },
			{ 0x67, "VK_NUMPAD7" },
			{ 0x68, "VK_NUMPAD8" },
			{ 0x69, "VK_NUMPAD9" },
			{ 0x6A, "VK_MULTIPLY" },
			{ 0x6B, "VK_ADD" },
			{ 0x6C, "VK_SEPARATOR" },
			{ 0x6D, "VK_SUBTRACT" },
			{ 0x6E, "VK_DECIMAL" },
			{ 0x6F, "VK_DIVIDE" },
			{ 0x70, "VK_F1" },
			{ 0x71, "VK_F2" },
			{ 0x72, "VK_F3" },
			{ 0x73, "VK_F4" },
			{ 0x74, "VK_F5" },
			{ 0x75, "VK_F6" },
			{ 0x76, "VK_F7" },
			{ 0x77, "VK_F8" },
			{ 0x78, "VK_F9" },
			{ 0x79, "VK_F10" },
			{ 0x7A, "VK_F11" },
			{ 0x7B, "VK_F12" },
			{ 0x7C, "VK_F13" },
			{ 0x7D, "VK_F14" },
			{ 0x7E, "VK_F15" },
			{ 0x7F, "VK_F16" },
			{ 0x80, "VK_F17" },
			{ 0x81, "VK_F18" },
			{ 0x82, "VK_F19" },
			{ 0x83, "VK_F19" },
			{ 0x84, "VK_F21" },
			{ 0x85, "VK_F22" },
			{ 0x86, "VK_F23" },
			{ 0x87, "VK_F24" },
			{ 0xBB, "VK_OEM_PLUS" },
			{ 0xBC, "VK_OEM_COMMA" },
			{ 0xBD, "VK_OEM_MINUS" },
			{ 0xBE, "VK_OEM_PERIOD" },
			{ 0xBA, "VK_OEM_1" },
			{ 0xBF, "VK_OEM_2" },
			{ 0xC0, "VK_OEM_3" },
			{ 0xDB, "VK_OEM_4" },
			{ 0xDC, "VK_OEM_5" },
			{ 0xDD, "VK_OEM_6" },
			{ 0xDE, "VK_OEM_7" },
			{ 0xDF, "VK_OEM_8" },
			{ 0xA6, "VK_BROWSER_BACK" },
			{ 0xA7, "VK_BROWSER_FORWARD" },
			{ 0xA8, "VK_BROWSER_REFRESH" },
			{ 0xA9, "VK_BROWSER_STOP" },
			{ 0xAA, "VK_BROWSER_SEARCH" },
			{ 0xAB, "VK_BROWSER_FAVORITES" },
			{ 0xAC, "VK_BROWSER_HOME" },
			{ 0xAD, "VK_VOLUME_MUTE" },
			{ 0xAF, "VK_VOLUME_UP" },
			{ 0xAE, "VK_VOLUME_DOWN" },
			{ 0xB0, "VK_MEDIA_NEXT_TRACK" },
			{ 0xB1, "VK_MEDIA_PREV_TRACK" },
			{ 0xB2, "VK_MEDIA_STOP" },
			{ 0xB3, "VK_MEDIA_PLAY_PAUSE" },
			{ 0xB4, "VK_LAUNCH_MAIL" },
			{ 0xB5, "VK_MEDIA_PREV_TRACK" },
			{ 0xB6, "VK_LAUNCH_APP1" },
			{ 0xB7, "VK_LAUNCH_APP2" },
			{ 0, kInvalidKeyName },
		} };
	} // namespace details

	/**
	 * \brief Возвращает название клавиши по ее номеру
	 * \detail Поддерживает только клавиши VK_*
	 * \param key Код клавиши
	 * \return Название клавиши или "&0", если для данной клавиши нет названия
	 */
	static constexpr std::string_view getKey( const int &key ) noexcept {
		const char *result = nullptr;
		if ( ( key >= 'A' && key <= 'Z' ) || ( key >= '0' && key <= '9' ) )
			result = reinterpret_cast<const char *>( &key );
		else {
			for ( auto &&alias : details::key_alias ) {
				if ( key == alias.key ) {
					result = alias.name;
					break;
				}
			}
			if ( result == nullptr ) { result = kInvalidKeyName; }
		}
		return result;
	}
	/**
	 * \brief Возвращает номер клавиши по ее названию
	 * \detail Поддерживает клавиши VK_* и номера в угловых скобках, например \<9\> == VK_TAB
	 * \param key Название клавиши
	 * \return Намер клавиши или 0, если номера для данного названия нет
	 */
	static constexpr int getKey( std::string_view key ) noexcept {
		int result = 0;

		if ( key.length() == 1 && ( ( key.front() >= 'A' && key.front() <= 'Z' ) || ( key.front() >= '0' && key.front() <= '9' ) ) )
			result = static_cast<unsigned char>( key.front() );
		else if ( key.length() > 2 && key.front() == '<' && key.back() == '>' ) {
			auto keyId = key.substr( 1, key.length() - 2 );
			for ( auto &&ch : keyId ) {
				if ( ch < '0' || ch > '9' ) {
					result = 0;
					break;
				}
				result = result * 10 + ( ch - '0' );
			}
		} else {
			for ( auto &&alias : details::key_alias ) {
				if ( key == alias.name ) {
					result = alias.key;
					break;
				}
			}
		}

		return result;
	}

	/**
	 * \brief Создает комбинацию клавиши из строки
	 * \param keys Строка с названиями клавиш, например "VK_CONTROL+VK_MENU+VK_DELETE"
	 * \param sep Символ разделяющий клавиши, например '+'
	 * \return Вектор номеров клавиш или пустой вектор, если из строки не удалось достать названия клавиш
	 */
	static std::vector<int> createCombo( std::string_view keys, char sep = '+' ) {
		std::vector<int> combo;

		size_t from = 0;
		while ( true ) {
			auto to = keys.find( sep, from );
			auto key = keys.substr( from, to - from );
			auto keyId = getKey( key );
			if ( keyId ) combo.push_back( keyId );
			if ( to != std::string_view::npos && to != keys.length() - 1 )
				from = to + 1;
			else
				break;
		}

		return combo;
	}

	/**
	 * \brief Преобразует KeyCombo в строку
	 * \param combo Комбинация клавиш
	 * \param sep Разделитель клавиш в будующей строке
	 * \return Строка
	 */
	static std::string combo2string( const std::vector<int> &combo, char sep = '+' ) {
		std::string str;

		for ( auto &key : combo ) {
			if ( !str.empty() ) str.push_back( sep );
			auto keyName = getKey( key );
			if ( !keyName.empty() && keyName != kInvalidKeyName )
				str += getKey( key );
			else {
				char buf[11]{ 0 };
				auto [ptr, ec] = std::to_chars( buf, buf + sizeof( buf ), key );
				if ( ec == std::errc() ) {
					str += "<";
					str += buf;
					str += ">";
				}
			}
		}

		return str;
	}

	/**
	 * \brief Проверяет валидна ли комбинация клавиш
	 * \param combo Комбинация клавиш
	 * \return true, если комбинация валидна
	 */
	static constexpr bool isComboValid( const std::vector<int> &combo ) noexcept {
		return std::ranges::all_of( combo, []( int key ) { return key != 0; } );
	}

	enum class GameKeysOnFoot : uint8_t
	{
		Left_or_Right = 0,
		Forward_or_Backward,
		SpecialCtrl_Left_or_Right,
		SpecialCtrl_Up_or_Down,
		Action_or_SecondaryFire,
		PrevWeapon_or_ZoomIn,
		AimWeapon,
		NextWeapon_or_ZoomOut,
		GroupCtrlForward,
		GroupCtrlBack,
		NO,
		YES,
		unused,
		ChangeCamera,
		Jump,
		EnterVehicle,
		Sprint,
		Fire,
		Crouch,
		LookBehind
	};
	enum class GameKeysVehicle : uint8_t
	{
		Left_or_Right = 0,
		Steer_Back_or_Up,
		SpecialCtrl_Left_or_Right,
		SpecialCtrl_Up_or_Down,
		SecondaryFire,
		LookLeft,
		HandBreak,
		LookRight,
		NextRadioStation,
		PreviousRadioStation,
		NO,
		YES_or_TripSkip,
		unused,
		Camera,
		Break_or_Reserve,
		Enter_or_Exit,
		Accelerate,
		Fire,
		Horn,
		Submission
	};
	union GameKeys {
		constexpr GameKeys( int keyId ) noexcept { number = keyId; }
		constexpr GameKeys( GameKeysOnFoot foot ) noexcept { onFoot = foot; }
		constexpr GameKeys( GameKeysVehicle veh ) noexcept { vehicle = veh; }
		GameKeysOnFoot onFoot;
		GameKeysVehicle vehicle;
		uint8_t number{};
	};
}; // namespace SRKeys
